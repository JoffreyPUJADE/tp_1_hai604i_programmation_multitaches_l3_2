#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>

#include <stdlib.h>

/* Programme client */

int main(int argc, char *argv[]) {

  /* je passe en paramètre l'adresse de la socket du serveur (IP et
     numéro de port) et un numéro de port à donner à la socket créée plus loin.*/

  /* Je teste le passage de parametres. Le nombre et la nature des
     paramètres sont à adapter en fonction des besoins. Sans ces
     paramètres, l'exécution doit être arrétée, autrement, elle
     aboutira à des erreurs.*/
  if (argc != 4){
    printf("utilisation : %s ip_serveur port_serveur port_client\n", argv[0]);
    exit(1);
  }

  /* Etape 1 : créer une socket */   
  int ds = socket(PF_INET, SOCK_DGRAM, 0);

  /* /!\ : Il est indispensable de tester les valeurs de retour de
     toutes les fonctions et agir en fonction des valeurs
     possibles. Voici un exemple */
  if (ds == -1){
    perror("Client : pb creation socket :");
    exit(1); // je choisis ici d'arrêter le programme car le reste
	     // dépendent de la réussite de la création de la socket.
  }
  
  /* J'ajoute des traces pour comprendre l'exécution et savoir
     localiser des éventuelles erreurs */
  printf("Client : creation de la socket réussie \n");
  
  // Je peux tester l'exécution de cette étape avant de passer à la
  // suite. Faire de même pour la suite : n'attendez pas de tout faire
  // avant de tester.
  
  /* Etape 2 : Nommer la socket du client */
  
  struct sockaddr_in ad;
  ad.sin_family = AF_INET;
  ad.sin_addr.s_addr = INADDR_ANY;
  ad.sin_port = htons((short)atoi(argv[3]));
  
  int res1 = bind(ds, (struct sockaddr*)&ad, sizeof(ad));
  
  if(res1 != 0)
  {
  	fprintf(stderr, "Une erreur s'est produite lors du nommage de la socket.\n");
  	exit(1);
  }
  
  /* Etape 3 : Désigner la socket du serveur */
  
  struct sockaddr_in socketDistante;
  socketDistante.sin_family = AF_INET;
  socketDistante.sin_addr.s_addr = inet_addr(argv[1]);
  socketDistante.sin_port = htons((short)atoi(argv[2]));
  
  /* Etape 4 : envoyer un message au serveur  (voir sujet pour plus de détails)*/
  
  char* message = malloc(sizeof(char) * 100);
  
  printf("Veuillez saisir un message de 100 caractères maximum.\n");
  fgets(message, 100, stdin);
  
  if(message == NULL)
  {
  	fprintf(stderr, "Une erreur système s'est produite lors de la saisie du message.\n");
  	exit(1);
  }
  
  ssize_t res2 = sendto(ds, (void*)message, strlen(message)+1, 0, (struct sockaddr*)&socketDistante, (socklen_t)sizeof(struct sockaddr_in));
  
  if(res2 == -1)
  {
  	fprintf(stderr, "Une erreur s'est produite lors de l'envoi du message au serveur.\n");
  	exit(1);
  }
  
  /* Etape 5 : recevoir un message du serveur (voir sujet pour plus de détails)*/
  
  int entierDuServeur = 0;
  socklen_t tailleTypeSocket = sizeof(struct sockaddr_in);
  
  res2 = recvfrom(ds, (void*)&entierDuServeur, (sizeof(int)+1), 0, (struct sockaddr*)&socketDistante, &tailleTypeSocket);
  
  if(res2 == -1)
  {
  	fprintf(stderr, "Une erreur s'est produite lors de la reception d'un message sur la socket.\n");
  	exit(1);
  }
  
  printf("Nombre recu : %d, du serveur : %s:%i.\n", entierDuServeur, inet_ntoa(socketDistante.sin_addr), ntohs(socketDistante.sin_port));
  
  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
  
  res1 = close(ds);
  
  if(res1 != 0)
  {
  	fprintf(stderr, "Une erreur s'est produite lors de la fermeture de la socket.\n");
  	exit(1);
  }
  
  
  printf("Client : je termine\n");
  return 0;
}

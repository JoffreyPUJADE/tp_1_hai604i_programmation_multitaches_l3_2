#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>

#include <time.h>

double duree(clock_t tDepart, clock_t tFin)
{
	return 1000.0 * (tFin - tDepart) / CLOCKS_PER_SEC;
}

/* Programme serveur */

int main(int argc, char *argv[]) {

  /* Je passe en paramètre le numéro de port qui sera donné à la socket créée plus loin.*/

  /* Je teste le passage de parametres. Le nombre et la nature des
     paramètres sont à adapter en fonction des besoins. Sans ces
     paramètres, l'exécution doit être arrétée, autrement, elle
     aboutira à des erreurs.*/
  if (argc != 2){
    printf("utilisation : %s port_serveur\n", argv[0]);
    exit(1);
  }

  /* Etape 1 : créer une socket */   
  int ds = socket(PF_INET, SOCK_DGRAM, 0);

  /* /!\ : Il est indispensable de tester les valeurs de retour de
     toutes les fonctions et agir en fonction des valeurs
     possibles. Voici un exemple */
  if (ds == -1){
    perror("Serveur : pb creation socket :");
    exit(1); // je choisis ici d'arrêter le programme car le reste
	     // dépendent de la réussite de la création de la socket.
  }
  
  /* J'ajoute des traces pour comprendre l'exécution et savoir
     localiser des éventuelles erreurs */
  printf("Serveur : creation de la socket réussie \n");
  
  // Je peux tester l'exécution de cette étape avant de passer à la
  // suite. Faire de même pour la suite : n'attendez pas de tout faire
  // avant de tester.
  
  /* Etape 2 : Nommer la socket du seveur */
  
  struct sockaddr_in ad;
  ad.sin_family = AF_INET;
  ad.sin_addr.s_addr = INADDR_ANY;
  ad.sin_port = htons((short)atoi(argv[1]));
  
  int res1 = bind(ds, (struct sockaddr*)&ad, sizeof(ad));
  
  if(res1 != 0)
  {
  	fprintf(stderr, "Une erreur s'est produite lors du nommage de la socket.\n");
  	exit(1);
  }
 
 while(1)
 {
	  /* Etape 4 : recevoir un message du client (voir sujet pour plus de détails)*/
	  
	  struct sockaddr_in aE;
	  socklen_t lg = sizeof(struct sockaddr_in);
	  
	  char messageRecu[300];
	  ssize_t res2 = recvfrom(ds, messageRecu, sizeof(messageRecu), 0, (struct sockaddr*)&aE, &lg);
	  
	  if(res1 == -1)
	  {
	  	fprintf(stderr, "Une erreur s'est produite lors de la reception d'un message sur la socket.\n");
	  	exit(1);
	  }
	  
	  /*char ipv4Client[INET_ADDRSTRLEN];
	  inet_ntop(AF_INET, &aE.sin_addr, ipv4Client, sizeof(ipv4Client));
	  
	  printf("Recu : %s, IP du client : %s.\n", messageRecu, ipv4Client);*/
	  
	  printf("Recu : %s, du client : %s:%i.\n", messageRecu, inet_ntoa(aE.sin_addr), ntohs(aE.sin_port));
	  
	  /* Etape 5 : envoyer un message au serveur (voir sujet pour plus de détails)*/
	  
	  struct sockaddr_in socketDistante;
	  socketDistante.sin_family = AF_INET;
	  socketDistante.sin_addr = aE.sin_addr;
	  socketDistante.sin_port = aE.sin_port;
	  
	  int nbOctets = res2;
	  
	  res2 = sendto(ds, (void*)&nbOctets, sizeof(int), 0, (struct sockaddr*)&socketDistante, (socklen_t)sizeof(struct sockaddr_in));
	  
	  if(res2 == -1)
	  {
	  	fprintf(stderr, "Une erreur s'est produite lors de l'envoi du message au client.\n");
	  	exit(1);
	  }
  }
  
  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
  
  res1 = close(ds);
  
  if(res1 != 0)
  {
  	fprintf(stderr, "Une erreur s'est produite lors de la fermeture de la socket.\n");
  	exit(1);
  }
  
  
  printf("Serveur : je termine\n");
  return 0;
}
